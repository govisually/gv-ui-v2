import Router from 'vue-router'
import Vue from 'vue'

function load (viewComponent) {
  return () => import(`@/views/${viewComponent}.vue`)
}

Vue.use(Router)
const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: load('Home'),
      meta: {
        requiresAuth: false
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: load('Login'),
      meta: {
        requiresAuth: false
      }
    }
  ]
})

export default router
