module.exports = {
  devServer: {
    // Displays overlay with warnings and errors
    overlay: {
      errors: true
    }
  },
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
    } else {
      // mutate for development...
     
    }
  },
  // Lints code when you save a file
  lintOnSave: true
}
